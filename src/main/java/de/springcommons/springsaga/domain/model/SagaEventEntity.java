package de.springcommons.springsaga.domain.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.UUID;

@MappedSuperclass
@Slf4j
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public abstract class SagaEventEntity {

    @Column(name = "command")
    private String command;

    @Column(name = "result")
    private String result;

    @Column(name = "successful")
    private boolean successful;

    @Column(name = "exception_id")
    private UUID exceptionId;

    @Column(name = "command_class")
    private String commandClassType;

    @Column(name = "result_class")
    private String resultClassType;

    public <T> Optional<T> getCommand() {
        return getObject(command, commandClassType);
    }

    protected <K> Optional<K> getObject(String input, String classType) {
        ObjectMapper jsonMapper = new ObjectMapper().findAndRegisterModules();
        try {
            //noinspection unchecked
            return (Optional<K>) Optional.of(jsonMapper.readValue(input, Class.forName(classType)));
        } catch (Exception e) {
            log.error("Can't map String to class! ErrorMessage: " + e.getMessage());
            return Optional.empty();
        }
    }

    public <R> Optional<R> getResult() {
        return getObject(result, resultClassType);
    }
}
