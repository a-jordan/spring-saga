package de.springcommons.springsaga.domain;

import de.springcommons.springsaga.domain.dto.sagaevent.FailureEvent;
import de.springcommons.springsaga.domain.dto.sagaevent.SagaEvent;
import de.springcommons.springsaga.domain.dto.sagaevent.SuccessEvent;
import de.springcommons.springsaga.domain.dto.sagastep.SagaStep;
import de.springcommons.springsaga.domain.mapper.SagaEventMapper;
import de.springcommons.springsaga.domain.model.SagaEventEntity;
import de.springcommons.springsaga.domain.repository.SagaEventStore;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;

import java.util.UUID;

/**
 * The abstract saga class, can be extended from specific saga implementations.
 * It provides the function to automatically store all {@link SagaEvent} into the provided {@link SagaEventStore}.
 *
 * @param <T> typing of the {@link SagaEventEntity} class.
 */
@RequiredArgsConstructor
public abstract class Saga<T extends SagaEventEntity> {

    public static final UUID sagaId = UUID.randomUUID();

    protected final SagaEventStore<T> eventStore;
    protected final SagaEventPublisher sagaEventPublisher;
    protected final SagaEventMapper<T> eventMapper;
    protected final SagaEventFactory eventFactory = new SagaEventFactory(sagaId);

    @Order(1)
    @EventListener(condition = "#event.sagaId eq T(de.springcommons.springsaga.domain.Saga).sagaId")
    public void persistSagaEvent(SuccessEvent<?> event) {
        T entity = eventMapper.map(event);
        eventStore.save(entity);
    }

    @Order(1)
    @EventListener(condition = "#event.sagaId eq T(de.springcommons.springsaga.domain.Saga).sagaId")
    public void persistSagaEvent(FailureEvent<?> event) {
        T entity = eventMapper.map(event);
        eventStore.save(entity);
    }

    protected int getRetryForSagaStep(SagaStep sagaStep, Long identifier) {
        return eventStore.getRetryCountForSagaStep(sagaStep, identifier);
    }
}
