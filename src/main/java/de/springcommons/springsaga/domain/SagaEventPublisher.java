package de.springcommons.springsaga.domain;

import de.springcommons.springsaga.domain.dto.sagaevent.SagaEvent;
import de.springcommons.springsaga.domain.dto.sagastep.SagaStepCommand;

/**
 * Interface which provide functionality to publish {@link SagaEvent} and {@link SagaStepCommand}.
 *
 * Basic Implementation in Spring Boot is to use the {@link org.springframework.context.ApplicationEventPublisher}.
 * Besides the application scope, other publishers can be implemented, such as message brokers to achieve horizontal
 * scaling across the saga.
 */
public interface SagaEventPublisher {

    void publishSagaEvent(SagaEvent event);

    void publishSagaStep(SagaStepCommand<?> sagaStepCommand);
}
