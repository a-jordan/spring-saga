package de.springcommons.springsaga.domain;

import de.springcommons.springsaga.domain.dto.sagaevent.FailureEvent;
import de.springcommons.springsaga.domain.dto.sagaevent.SagaEvent;
import de.springcommons.springsaga.domain.dto.sagaevent.SuccessEvent;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

/**
 * Class which provides methods to create {@link SagaEvent} relating to the
 * {@link Saga} from which it is published.
 */
@RequiredArgsConstructor
public class SagaEventFactory {

    private final UUID sagaId;

    public <K, R extends SuccessEvent<K>, B extends SuccessEvent.SuccessEventBuilder<K, R, B>> SuccessEvent.SuccessEventBuilder<K, R, B>
    createSagaEvent(SuccessEvent.SuccessEventBuilder<K, R, B> builder) {
        return builder.sagaId(sagaId);
    }

    public <K, R extends FailureEvent<K>, B extends FailureEvent.FailureEventBuilder<K, R, B>> FailureEvent.FailureEventBuilder<K, R, B>
    createSagaEvent(FailureEvent.FailureEventBuilder<K, R, B> builder) {
        return builder.sagaId(sagaId);
    }
}
