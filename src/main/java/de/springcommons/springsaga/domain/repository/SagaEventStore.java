package de.springcommons.springsaga.domain.repository;


import de.springcommons.springsaga.domain.dto.sagastep.SagaStep;
import de.springcommons.springsaga.domain.model.SagaEventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Repository interface for persisting saga step results.
 *
 * @param <T> typing of event entity
 */
@NoRepositoryBean
public interface SagaEventStore<T extends SagaEventEntity> extends JpaRepository<T, Long> {

    int getRetryCountForSagaStep(SagaStep sagaStep, Long identifier);
}
