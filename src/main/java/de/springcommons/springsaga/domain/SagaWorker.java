package de.springcommons.springsaga.domain;

import de.springcommons.springsaga.common.exception.SagaTechnicalException;
import de.springcommons.springsaga.domain.dto.sagaevent.FailureEvent;
import de.springcommons.springsaga.domain.dto.sagaevent.SagaEvent;
import de.springcommons.springsaga.domain.dto.sagaevent.SuccessEvent;
import de.springcommons.springsaga.domain.dto.sagastep.SagaStepCommand;
import de.springcommons.springsaga.domain.mapper.EventProvider;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * The Saga Worker provides the logic to execute {@link SagaStepCommand} and publish
 * {@link SuccessEvent} or {@link FailureEvent}  accordingly,
 * which can be further processed in the {@link Saga}.
 *
 * @param <T> typing for the internal used Mono.
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SagaWorker<T> {

    private static final String LOG_MESSAGE = "[SagaStep]. SagaId: {}, SagaStep: {}, EventId: {}";
    private final List<EventProvider<T, ? extends SagaEvent>> successEvents = new ArrayList<>();
    private final List<EventProvider<SagaTechnicalException, ? extends SagaEvent>> failureEvents = new ArrayList<>();
    private Supplier<T> supplier;

    public static <U> SagaWorker<U> apply(Supplier<U> supplier) {
        SagaWorker<U> mono = new SagaWorker<>();
        mono.supplier = supplier;
        return mono;
    }

    public void logSagaEvent(SagaEvent event) {
        if (event.isSuccessful()) {
            logSuccessfulSagaEvent(event);
        } else {
            logFailedSagaEvent(event);
        }
    }

    private void logSuccessfulSagaEvent(SagaEvent event) {
        log.debug("Success " + LOG_MESSAGE,
                event.getSagaId(), event.getSagaStep().getValue(), event.getIdentifier()
        );
    }

    private void logFailedSagaEvent(SagaEvent event) {
        log.error("Failure " + LOG_MESSAGE,
                event.getSagaId(), event.getSagaStep().getValue(), event.getIdentifier()
        );
    }

    public SagaWorker<T> setOnSuccessEvent(EventProvider<T, ?> eventProvider) {
        successEvents.add(eventProvider);
        return this;
    }

    public SagaWorker<T> setOnFailureEvent(EventProvider<SagaTechnicalException, ?> eventProvider) {
        failureEvents.add(eventProvider);
        return this;
    }

    public void whenCompletedPublishWith(SagaEventPublisher sagaEventPublisher) {
        Mono.fromCallable(() -> supplier.get())
                .subscribe(response -> {
                    for (EventProvider<T, ? extends SagaEvent> provider : successEvents) {
                        SagaEvent event = provider.createEvent(response);
                        logSagaEvent(event);
                        sagaEventPublisher.publishSagaEvent(event);
                    }
                    successEvents.clear();
                }, throwable -> {
                    SagaTechnicalException exception = new SagaTechnicalException(throwable);
                    log.error(exception.getMessage());
                    exception.printStackTrace();

                    for (EventProvider<SagaTechnicalException, ? extends SagaEvent> provider : failureEvents) {
                        SagaEvent event = provider.createEvent(exception);
                        logSagaEvent(event);
                        sagaEventPublisher.publishSagaEvent(provider.createEvent(exception));
                    }
                    failureEvents.clear();
                });
    }
}
