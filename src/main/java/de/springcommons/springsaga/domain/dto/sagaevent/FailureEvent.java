package de.springcommons.springsaga.domain.dto.sagaevent;

import de.springcommons.springsaga.common.exception.SagaTechnicalException;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

/**
 * Saga Failure event which should be published, when a saga step execution was not successful.
 *
 * @param <T> typing of
 */
@Getter
@ToString(callSuper = true)
@SuperBuilder
public abstract class FailureEvent<T> extends SagaEvent {

    protected final T command;
    protected final SagaTechnicalException exception;

    @Override
    protected boolean setSuccessful() {
        return false;
    }

    public String getCommandClassType() {
        return command.getClass().getName();
    }
}
