package de.springcommons.springsaga.domain.dto.sagastep;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@RequiredArgsConstructor
@SuperBuilder
public abstract class SagaStepCommand<T> {

    private final T command;

    private final SagaStep sagaStep;
}
