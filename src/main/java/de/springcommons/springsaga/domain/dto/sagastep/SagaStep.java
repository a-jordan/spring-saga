package de.springcommons.springsaga.domain.dto.sagastep;

public interface SagaStep {

    String getValue();
}
