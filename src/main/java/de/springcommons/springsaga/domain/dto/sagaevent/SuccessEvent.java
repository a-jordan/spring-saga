package de.springcommons.springsaga.domain.dto.sagaevent;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@ToString(callSuper = true)
public abstract class SuccessEvent<T> extends SagaEvent {

    private final T result;

    @Override
    protected boolean setSuccessful() {
        return true;
    }

    public String getResultClassType() {
        return result.getClass().getName();
    }
}
