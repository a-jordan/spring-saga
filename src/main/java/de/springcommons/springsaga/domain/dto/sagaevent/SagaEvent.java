package de.springcommons.springsaga.domain.dto.sagaevent;

import de.springcommons.springsaga.domain.dto.sagastep.SagaStep;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

/**
 * Basic Saga Event class with base properties.
 */
@RequiredArgsConstructor
@Getter
@NonNull
@ToString
@SuperBuilder
public abstract class SagaEvent {

    protected final UUID sagaId;
    protected final Long identifier;
    protected final SagaStep sagaStep = setSagaStep();
    protected final boolean successful = setSuccessful();

    protected abstract SagaStep setSagaStep();

    protected abstract boolean setSuccessful();
}

