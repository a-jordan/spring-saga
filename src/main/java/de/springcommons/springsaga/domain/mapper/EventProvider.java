package de.springcommons.springsaga.domain.mapper;

import de.springcommons.springsaga.domain.dto.sagaevent.SagaEvent;

/**
 * Functional Interface to provide a specific {@link SagaEvent}.
 *
 * @param <T> input data for the requested {@link SagaEvent}
 * @param <R> typing of result {@link SagaEvent}
 */
@FunctionalInterface
public interface EventProvider<T, R extends SagaEvent> {

    R createEvent(T value);
}
