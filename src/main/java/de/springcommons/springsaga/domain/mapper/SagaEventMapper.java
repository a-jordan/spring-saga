package de.springcommons.springsaga.domain.mapper;


import com.fasterxml.jackson.databind.ObjectMapper;
import de.springcommons.springsaga.domain.dto.sagaevent.FailureEvent;
import de.springcommons.springsaga.domain.dto.sagaevent.SagaEvent;
import de.springcommons.springsaga.domain.dto.sagaevent.SuccessEvent;
import de.springcommons.springsaga.domain.model.SagaEventEntity;
import de.springcommons.springsaga.domain.repository.SagaEventStore;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Abstract class that provides functionality to map specific {@link SagaEvent}
 * to {@link SagaEventEntity} to persist it via the {@link SagaEventStore}.
 *
 * @param <T> typing of the {@link SagaEventEntity}.
 */
@RequiredArgsConstructor
@Slf4j
public abstract class SagaEventMapper<T extends SagaEventEntity> {

    protected final ObjectMapper jsonMapper;

    public abstract <R> T map(SuccessEvent<R> event);

    public abstract <R> T map(FailureEvent<R> event);
}
