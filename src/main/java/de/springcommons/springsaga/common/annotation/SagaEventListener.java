package de.springcommons.springsaga.common.annotation;

import de.springcommons.springsaga.domain.dto.sagaevent.SagaEvent;
import org.springframework.context.event.EventListener;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to mark methods that should listen for {@link SagaEvent}
 */
@EventListener
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SagaEventListener {

}
