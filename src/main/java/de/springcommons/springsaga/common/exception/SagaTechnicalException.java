package de.springcommons.springsaga.common.exception;

import java.util.UUID;

/**
 * Specific exception class for exceptions within the saga execution.
 */
public class SagaTechnicalException extends RuntimeException {

    private final UUID id = UUID.randomUUID();

    public SagaTechnicalException(Throwable cause) {
        super(cause);
    }

    @Override
    public String getMessage() {
        return "UUID: " + this.id + " Message: " + super.getMessage();
    }
}
