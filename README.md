# Spring Saga

A **lightweight Java Spring Boot library** which is oriented on the Saga pattern to solve the problem of large process chains in microservice architectures. Possible use case are:
- _Booking process chains_, e.g. vacation bookings including flight/hotel and rental car, where several processes have to be triggered via one platform in order to achieve a successful overall booking.
- _Order entry in logistics applications_, where e.g. several checks (e.g. inventory checks), which requires information from other microservices before an order can be released for processing
- _..._

## Concept

The main concept is based on event driven design. Events enable a looser coupling and provide the basis for dividing large processes as far as possible into small methods, which are, however, not coupled with each other.

The concept is explained using the example of an _order entry process_ in a warehouse management system. This process requires the creation of many new db entities and also requires information from other microservices. Now, this overall process must be transformed into a saga, i.e., it must be divided into smaller subprocesses, each of which represents a partial status in the order entry process after completion. 
The order process is divided as follows:
- Order entry 
- Inventory check (external microservice)
- stock reservations
- Order release

These steps can now be more or less transferred to the technical saga in the code. They are represented in the code as class **SagaStep**. The execution of a SagaStep is done in an encapsulated method. Here, the execution can be successful or unsuccessful in case of an exception. For this purpose, a **SagaEvent** is published after each completion of a SagaStep, which notifies about the success or failure of the SagaStep. By corresponding EventListeners on these SagaEvents, in case of a successfully executed SagaStep the next SagaStep can be executed, or in case of an error e.g. notifications to admins or automatic retries can be made.

![Saga Concept](saga_concept.svg)

In the test package you can find a code implementation of this example saga to understand how to use the classes and interfaces of this library.

Within the application, the **ApplicationEventPublisher** provided by Spring Boot is used to publish SagaSteps and SagaEvents. To establish asynchronous external communication to other microservices, the **ExternalGateway** class can be implemented accordingly. A possible implementation is e.g. a MessageBroker like RabbitMQ to publish messages to other microservices.  With the return of a message, which comes in via the corresponding EntryPoint of the Saga, the subsequent SagaStep can be triggered.

